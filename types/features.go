package types

type Features map[string]float64


func FeatureSum(list map[string]float64) float64 {

    var sum float64 = 0

    for _, value := range list {
        sum += value
    }

    return sum
}