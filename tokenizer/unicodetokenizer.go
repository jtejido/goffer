package tokenizer

import (
	"strings"
	. "bitbucket.org/jtejido/goffer/types"
	. "github.com/blevesearch/segment"
	"bufio"
)

type UnicodeTokenizer struct {}

func NewUnicodeTokenizer() *UnicodeTokenizer {
	return new(UnicodeTokenizer)
}

func (t UnicodeTokenizer) Tokenize(text string) Tokens {
	tokens := make(Tokens)
	scanner := bufio.NewScanner(strings.NewReader(text))
	scanner.Split(SplitWords)
	ctr := 0
	for scanner.Scan() {
		text := scanner.Bytes()
		tokens[ctr] = text
		ctr++
	}

	return tokens

}
