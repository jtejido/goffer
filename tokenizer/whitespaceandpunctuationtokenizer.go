package tokenizer

import (
	"strings"
	. "bitbucket.org/jtejido/goffer/types"
	"unicode"
	"unicode/utf8"
	"bufio"

)

type WhitespaceAndPunctuationTokenizer struct {}

func NewWhitespaceAndPunctuationTokenizer() *WhitespaceAndPunctuationTokenizer {
	return new(WhitespaceAndPunctuationTokenizer)
}

func (t WhitespaceAndPunctuationTokenizer) Tokenize(text string) Tokens {
	tokens := make(Tokens)
	scanner := bufio.NewScanner(strings.NewReader(text))
	scanner.Split(t.SplitToken)
	ctr := 0
	for scanner.Scan() {
		text := scanner.Bytes()
		tokens[ctr] = text
		ctr++
	}

	return tokens

}

func (t WhitespaceAndPunctuationTokenizer) SplitToken(data []byte, atEOF bool) (advance int, token []byte, err error) {
	var class func(r rune) bool
	for width, i := 0, 0; i < len(data); i += width {
		var r rune
		r, width = utf8.DecodeRune(data[i:])

		if class == nil {
			switch {
			case unicode.IsLetter(r):
				class = unicode.IsLetter
			case unicode.IsSpace(r):
				class = unicode.IsSpace
			case unicode.IsPunct(r):
				class = unicode.IsPunct
			default:
				class = func(r rune) bool {
					return !unicode.IsLetter(r) && !unicode.IsPunct(r)
				}
			}
		}

		if !class(r) {
			return i, data[:i], nil
		}
	}

	if atEOF && len(data) > 0 {
		return len(data), data[:], nil
	}

	return 0, nil, nil
}


