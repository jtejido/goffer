package feature

import (
	. "bitbucket.org/jtejido/goffer/index"
	. "bitbucket.org/jtejido/goffer/feature"
	. "bitbucket.org/jtejido/goffer/model"
)

type Transformer struct {
	doc          FeatureInterface
	indexManager *IndexManager
	model        WeightedModelInterface
}

func NewTransformer(imanager *IndexManager, model WeightedModelInterface, vector FeatureInterface) *Transformer {
	t := new(Transformer)
	t.doc = vector
	t.indexManager = imanager
	t.model = model
	return t
}

func (t Transformer) GetFeature() FeatureVector {
	feature := make(FeatureVector)

	tokenSum := t.doc.GetLength()

	tokenCount := t.doc.GetCount()

	for str, value := range t.doc.GetFeature() {
		stats := t.indexManager.Search(str)

		feature[str] = 0

		if stats != nil {
			t.model.GetBaseModel().SetStats(stats)
			feature[str] += t.model.GetScore(value, tokenSum, tokenCount)
		}

	}

	return feature
}

