package index

import (
	"bytes"
	"encoding/gob"
	. "bitbucket.org/jtejido/goffer/util/file"
)

type IndexReader struct {
	indexManager *IndexManager
	index        *Index
	path         string
}

func NewIndexReader(path string) (*IndexReader, error) {
	i := new(IndexReader)
	i.path = path
	i_temp, err := i.Fread(path)
	if err != nil {
		return nil, err
	}
	i.index = i_temp
	i.indexManager = NewIndexManager(i.index)
	return i, nil
}

func (ireader *IndexReader) GetIndexManager() *IndexManager {
	return ireader.indexManager
}

func (ireader *IndexReader) GetIndex() *Index {
	return ireader.index
}

func (ireader *IndexReader) Fread(path string) (*Index, error) {

	file, err := Fopen(path)

	if err != nil {
		return nil, err
	}

	m := NewIndex()


	if err != nil {
		return nil, err
	}

	b := bytes.Buffer{}
	b.WriteString(file)
	d := gob.NewDecoder(&b)
	err = d.Decode(&m)

	if err != nil {
		return nil, err
	}

	return m, nil
}
