package index

import (
	"bytes"
	"encoding/gob"
	. "bitbucket.org/jtejido/goffer/statistics"
)

type IndexEntry struct {
	value *EntryStatistics
}

func NewIndexEntry(value *EntryStatistics) *IndexEntry {
	indexentry := new(IndexEntry)
	indexentry.value = value
	return indexentry
}

func (indexentry IndexEntry) GetValue() *EntryStatistics {
	return indexentry.value
}

func (indexentry *IndexEntry) SetValue(value *EntryStatistics) {
	indexentry.value = value
}

// Satisfying GobEncoder Interface from encoding/gob
func (indexentry *IndexEntry) GobEncode() ([]byte, error) {
	w := new(bytes.Buffer)
	encoder := gob.NewEncoder(w)
	err := encoder.Encode(indexentry.value)
	if err != nil {
		return nil, err
	}

	return w.Bytes(), nil
}

// Satisfying GobDecoder Interface from encoding/gob
func (indexentry *IndexEntry) GobDecode(buf []byte) error {
	r := bytes.NewBuffer(buf)
	decoder := gob.NewDecoder(r)

	return decoder.Decode(&indexentry.value)
}
