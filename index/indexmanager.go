package index

import (
	. "bitbucket.org/jtejido/goffer/collection"
	. "bitbucket.org/jtejido/goffer/feature"
	. "bitbucket.org/jtejido/goffer/metadata"
	. "bitbucket.org/jtejido/goffer/statistics"
	. "bitbucket.org/jtejido/goffer/util/maps"
	"sync"
)

type IndexManager struct {
	collection Collection
	index      *Index
}

func NewIndexManager(index *Index) *IndexManager {
	indexmanager := new(IndexManager)
	indexmanager.index = index
	return indexmanager
}

var (
	totalByTermPresence = make(map[string]float64)
	uniqueTotalByTermPresence = make(map[string]float64)
	termFrequency = make(map[string]float64)
	documentFrequency = make(map[string]float64)
	postinglist = make(map[string]map[int][]int)
	flag = make(map[int]map[string]bool)
)

func (indexmanager *IndexManager) BuildIndex() {
	var numberofDocuments = 0
	var wg sync.WaitGroup
	wg.Add(len(indexmanager.collection))
    go func() {
		for id, v := range indexmanager.collection {
				defer wg.Done()
	        	if flag[id] == nil {
			        flag[id] = make(map[string]bool)
			    }
				indexmanager.index.AddMetadata(id, v.GetMetaData())
				numberofDocuments++
				tokens := v.CountValues()
				tokensCount := tokens.GetCount()
				tokensSum := tokens.GetLength()
				var subtask sync.WaitGroup
				subtask.Add(len(v.GetDocument()))
	   			go func() {
					for offset, b := range v.GetDocument() {
						defer subtask.Done()
						str := string(b)
		        		if postinglist[str] == nil {
							postinglist[str] = make(map[int][]int)
						}
		        		postinglist[str][id] = append(postinglist[str][id], offset)
		        		v_fl, ok_fl := flag[id][str]
						if ok_fl {
							if v_fl {
								flag[id][str] = true
							} else {
								flag[id][str] = false
							}
						}
		        		_, ok_termfreq := termFrequency[str]
						if ok_termfreq {
							termFrequency[str] ++
						} else {
							termFrequency[str] = 1
						}
		        		_, ok_df := documentFrequency[str]
						if ok_df {
							if !flag[id][str] {
								flag[id][str] = true
								totalByTermPresence[str] += tokensSum
								uniqueTotalByTermPresence[str] += tokensCount
								documentFrequency[str]++
							}
						} else {
							flag[id][str] = true
							totalByTermPresence[str] = tokensSum
							uniqueTotalByTermPresence[str] = tokensCount
							documentFrequency[str] = 1
						}

					}
				}()
				subtask.Wait()
	    }

	}()
	wg.Wait()

	Tf_sum := Map_sum(termFrequency)
	collectionStats := NewCollectionStatistics()
	collectionStats.SetNumberOfDocuments(float64(numberofDocuments))
	collectionStats.SetAverageDocumentLength(float64(Tf_sum / float64(numberofDocuments)))
	collectionStats.SetNumberOfTokens(float64(Tf_sum))
	collectionStats.SetNumberOfUniqueTokens(float64(len(termFrequency)))
	indexmanager.index.SetCollectionStatistics(collectionStats)

	for st, string_v := range termFrequency {
		entry := NewEntryStatistics()
		entry.SetTermFrequency(string_v)
		entry.SetDocumentFrequency(documentFrequency[st])
		entry.SetTotalByTermPresence(totalByTermPresence[st])
		entry.SetUniqueTotalByTermPresence(uniqueTotalByTermPresence[st])
		post := make(PostingStatistics)
		for pl_k, pl_v := range postinglist[st] {
			post[pl_k] = pl_v
			entry.SetPostingList(post)
		}

		indexmanager.index.AddEntry(st, entry)
	}

}

func (indexmanager *IndexManager) SetCollection(collection Collection) {
	indexmanager.collection = collection
}

func (indexmanager IndexManager) Search(key string) *EntryStatistics {
	data := indexmanager.index.GetData()
	v, ok := data[key]
	if ok {
		return v.GetValue()
	}

	return nil
}

func (indexmanager IndexManager) GetMatrix() map[int]FeatureVector {
	data := indexmanager.index.GetData()
	collection := make(map[int]FeatureVector)
	for term, sub := range data {
		array := sub.GetValue().GetPostingList()
		for id, val := range array {
			if collection[id] == nil {
				collection[id] = make(FeatureVector)
			}
			collection[id][term] = val.GetTf()
		}
	}

	return collection
}

func (indexmanager IndexManager) GetMetaData(id int) MetaData {
    metadata := indexmanager.index.GetMetadata()

	return metadata[id]
}

func (indexmanager IndexManager) GetDocumentVector(id int) FeatureVector {
	documents := indexmanager.GetMatrix()

	return documents[id]
}

func (indexmanager IndexManager) GetCollectionStatistics() *CollectionStatistics {
	return indexmanager.index.GetCollectionStatistics()
}

func (indexmanager IndexManager) GetData() *Index {
	return indexmanager.index
}
