package index

import (
	"bytes"
	"encoding/gob"
	"fmt"
	. "bitbucket.org/jtejido/goffer/collection"
	. "bitbucket.org/jtejido/goffer/document"
	. "bitbucket.org/jtejido/goffer/util"
	"os"
)

const (
	Extension = ".idx"
	Filename  = "basset_index"
	Directory = "index/"
)

var open bool

type IndexWriter struct {
	transformer         TransformationInterface
	collection          Collection
	directory, filename string
}

func NewIndexWriter(directory string) (*IndexWriter, error) {
	i := new(IndexWriter)
	i.collection = make(Collection)
	open = true
	if len(directory) <= 0 {
		return nil, fmt.Errorf("Please enter a directory path.")
	}

	if _, err := os.Stat(directory); os.IsNotExist(err) {
		e := os.Mkdir(directory, 0755)
		if e != nil {
			return nil, e
		}
	}

	return i, nil
}

func (iwriter *IndexWriter) AddDocument(document Document) error {

	err := iwriter.EnsureOpen()

	if err != nil {
		return err
	}

	iwriter.collection.AddDocument(document)

	return nil

}

func (iwriter *IndexWriter) ApplyTransformation(transformer TransformationInterface) error {

	err := iwriter.EnsureOpen()

	if err != nil {
		return err
	}

	iwriter.transformer = transformer

	return nil
}

func (iwriter *IndexWriter) Close() error {
	err := iwriter.EnsureOpen()

	if err != nil {
		return err
	}

	if iwriter.transformer != nil {
		iwriter.collection.ApplyTransformation(iwriter.transformer)
	}

	index, i_err := iwriter.StartLexicalIndex(iwriter.collection)

	if i_err != nil {
		return i_err
	}

	write_err := iwriter.WriteIndex(index)

	if write_err != nil {
		return write_err
	}

	open = false

	return nil
}

func (iwriter *IndexWriter) WriteIndex(index *Index) error {
	err := iwriter.EnsureOpen()

	if err != nil {
		return err
	}

	f_err := iwriter.Fwrite(index)

	if f_err != nil {
		return f_err
	}

	return nil
}

func (iwriter *IndexWriter) StartLexicalIndex(collection Collection) (*Index, error) {
	err := iwriter.EnsureOpen()

	if err != nil {
		return nil, err
	}

	manager := NewIndexManager(NewIndex())
	manager.SetCollection(iwriter.collection)
	manager.BuildIndex()
	return manager.GetData(), nil
}

func (iwriter *IndexWriter) EnsureOpen() error {

	if !open {
		return fmt.Errorf("Index files have been commited. Please start a new instance of IndexWriter.")
	}

	return nil
}

func (iwriter *IndexWriter) Fwrite(index *Index) error {

	b := bytes.Buffer{}
	e := gob.NewEncoder(&b)
	err := e.Encode(index)

	if err != nil {
		return err
	}

	file, err := os.Create(Directory + Filename + Extension)

	if err != nil {
		os.Exit(0)
		return err
	}

	defer file.Close()

	_, err = file.Write(b.Bytes())

	if err != nil {
		return err
	}

	// save changes
	err = file.Sync()

	if err != nil {
		return err
	}

	return nil
}
