package index

import (
	"bytes"
	"encoding/gob"
	. "bitbucket.org/jtejido/goffer/metadata"
	. "bitbucket.org/jtejido/goffer/statistics"
)

type Index struct {
	postings        map[int]MetaData
	entries         map[string]*IndexEntry
	collectionStats *CollectionStatistics
}

func NewIndex() *Index {
	index := new(Index)
	index.postings = make(map[int]MetaData)
	index.entries = make(map[string]*IndexEntry)
	return index
}

func (index *Index) AddMetadata(id int, metadata MetaData) {
	index.postings[id] = metadata
}

func (index Index) GetData() map[string]*IndexEntry {
	return index.entries
}

func (index Index) GetMetadata() map[int]MetaData {
	return index.postings
}

func (index *Index) AddEntry(key string, value *EntryStatistics) {
	index.entries[key] = NewIndexEntry(value)
}

func (index *Index) SetCollectionStatistics(collectionStats *CollectionStatistics) {
	index.collectionStats = collectionStats
}

func (index Index) GetCollectionStatistics() *CollectionStatistics {
	return index.collectionStats
}

// Satisfying GobEncoder Interface from encoding/gob
func (index *Index) GobEncode() ([]byte, error) {
	w := new(bytes.Buffer)
	encoder := gob.NewEncoder(w)
	err_e := encoder.Encode(index.entries)
	if err_e != nil {
		return nil, err_e
	}
	err_p := encoder.Encode(index.postings)
	if err_p != nil {
		return nil, err_p
	}
	err_cs := encoder.Encode(index.collectionStats)
	if err_cs != nil {
		return nil, err_cs
	}

	return w.Bytes(), nil
}

// Satisfying GobDecoder Interface from encoding/gob
func (index *Index) GobDecode(buf []byte) error {
	r := bytes.NewBuffer(buf)
	decoder := gob.NewDecoder(r)
	err_e := decoder.Decode(&index.entries)
	if err_e != nil {
		return err_e
	}
	err_p := decoder.Decode(&index.postings)
	if err_p != nil {
		return err_p
	}

	return decoder.Decode(&index.collectionStats)
}
