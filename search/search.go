package search

import (
	"fmt"
	. "bitbucket.org/jtejido/goffer/document"
	. "bitbucket.org/jtejido/goffer/feature"
	. "bitbucket.org/jtejido/goffer/index"
	. "bitbucket.org/jtejido/goffer/model"
	. "bitbucket.org/jtejido/goffer/similarity"
	. "bitbucket.org/jtejido/goffer/statistics"
	. "bitbucket.org/jtejido/goffer/transformer"
)

type Search struct {
	indexmanager *IndexManager
	model        WeightedModelInterface
	query        FeatureVector
}

func NewSearch(ireader *IndexReader) *Search {
	search := new(Search)
	search.indexmanager = ireader.GetIndexManager()
	search.model = nil
	search.query = make(FeatureVector)
	return search
}

func (search Search) GetIndexManager() *IndexManager {
	return search.indexmanager
}

func (search Search) GetMatrix() map[int]FeatureVector {
	return search.GetIndexManager().GetMatrix()
}

func (search Search) GetDocumentVector(id int) FeatureVector {
	return search.GetIndexManager().GetDocumentVector(id)
}

func (search Search) GetCollectionStatistics() *CollectionStatistics {
	return search.GetIndexManager().GetCollectionStatistics()
}

// We set the model using this method, at this point, we'll assume that the QueryModel is initiated by this model, so we set the stats for both.
func (search *Search) Model(model WeightedModelInterface) error {
	if len(search.GetQuery().GetFeature()) == 0 {
		return fmt.Errorf("Please set a Query document first.")
	}

	model.GetBaseModel().SetCollectionStatistics(search.GetCollectionStatistics())
	model.GetBaseModel().GetQueryModel().GetBaseModel().SetCollectionStatistics(search.GetCollectionStatistics())
	search.model = model
	return nil
}

func (search Search) GetModel() WeightedModelInterface {
	return search.model
}

func (search *Search) QueryModel(model WeightedModelInterface) error {
	if search.GetModel() == nil {
		return fmt.Errorf("Please set a Model first.")
	}
	model.GetBaseModel().SetCollectionStatistics(search.GetCollectionStatistics())
	search.GetModel().GetBaseModel().SetQueryModel(model)
	return nil
}

func (search Search) GetQueryModel() (WeightedModelInterface, error) {
	if search.GetModel() == nil {
		return nil, fmt.Errorf("Please set a Model first.")
	}

	return search.model.GetBaseModel().GetQueryModel(), nil
}

func (search *Search) Similarity(similarity SimilarityInterface) {
	search.GetModel().GetBaseModel().SetSimilarity(similarity)
}

func (search Search) GetSimilarity() SimilarityInterface {
	return search.GetModel().GetBaseModel().GetSimilarity()
}

func (search *Search) Query(document DocumentInterface) {
	search.query = FeatureVector(document.GetDocument().CountValues())
}

func (search Search) GetQuery() FeatureVector {
	return search.query
}

func (search Search) Search() (map[string]float64, error) {
	qm, err := search.GetQueryModel()
	if err != nil {
		return nil, err
	}
	queryVec := search.TransformVector(qm, search.GetQuery())
	results := search.GetResults(queryVec)

	return results, nil
}

func (search Search) TransformVector(model WeightedModelInterface, vector FeatureInterface) FeatureInterface {
	docFeature := NewTransformer(search.GetIndexManager(), model, vector)
	return docFeature.GetFeature()
}

func (search Search) GetResults(queryVector FeatureInterface) map[string]float64 {
	results := make(map[string]float64)
	for id, doc := range search.GetMatrix() {
		docVector := search.TransformVector(search.GetModel(), doc)
		score := search.GetScore(queryVector.GetFeature(), docVector.GetFeature())
		title, _ := search.GetIndexManager().GetMetaData(id).GetTag("title")
		results[title] = score
	}

	return results
}

func (search Search) GetScore(a FeatureVector, b FeatureVector) float64 {
	return search.GetSimilarity().Similarity(a, b)
}
