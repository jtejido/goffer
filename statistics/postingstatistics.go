package statistics

// PostingStatistics makes the Index type a full-inverted index, it represents the listing term offset in a document
type PostingStatistics map[int]OffsetList


type OffsetList []int

func (os OffsetList) GetTf() float64 {
	return float64(len(os))
}


