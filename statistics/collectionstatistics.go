package statistics

import (
	"bytes"
	"encoding/gob"
)

type CollectionStatistics struct {
	numberOfDocuments     float64
	averageDocumentLength float64
	numberOfTokens        float64
	numberOfUniqueTokens  float64
}

func NewCollectionStatistics() *CollectionStatistics {
	cs := new(CollectionStatistics)
	return cs
}

func (cs CollectionStatistics) GetNumberOfDocuments() float64 {
	return cs.numberOfDocuments
}

func (cs CollectionStatistics) GetAverageDocumentLength() float64 {
	return cs.averageDocumentLength
}

func (cs CollectionStatistics) GetNumberOfTokens() float64 {
	return cs.numberOfTokens
}

func (cs CollectionStatistics) GetNumberOfUniqueTokens() float64 {
	return cs.numberOfUniqueTokens
}

func (cs *CollectionStatistics) SetNumberOfDocuments(value float64) {
	cs.numberOfDocuments = value
}

func (cs *CollectionStatistics) SetAverageDocumentLength(value float64) {

	cs.averageDocumentLength = value
}

func (cs *CollectionStatistics) SetNumberOfTokens(value float64) {
	cs.numberOfTokens = value
}

func (cs *CollectionStatistics) SetNumberOfUniqueTokens(value float64) {
	cs.numberOfUniqueTokens = value
}

// Satisfying GobEncoder Interface from encoding/gob
func (cs *CollectionStatistics) GobEncode() ([]byte, error) {
	w := new(bytes.Buffer)
	encoder := gob.NewEncoder(w)
	err_nd := encoder.Encode(cs.numberOfDocuments)
	if err_nd != nil {
		return nil, err_nd
	}
	err_adl := encoder.Encode(cs.averageDocumentLength)
	if err_adl != nil {
		return nil, err_adl
	}
	err_nt := encoder.Encode(cs.numberOfTokens)
	if err_nt != nil {
		return nil, err_nt
	}
	err_nut := encoder.Encode(cs.numberOfUniqueTokens)
	if err_nut != nil {
		return nil, err_nut
	}

	return w.Bytes(), nil
}

// Satisfying GobDecoder Interface from encoding/gob
func (cs *CollectionStatistics) GobDecode(buf []byte) error {
	r := bytes.NewBuffer(buf)
	decoder := gob.NewDecoder(r)
	err_nd := decoder.Decode(&cs.numberOfDocuments)
	if err_nd != nil {
		return err_nd
	}
	err_adl := decoder.Decode(&cs.averageDocumentLength)
	if err_adl != nil {
		return err_adl
	}
	err_nt := decoder.Decode(&cs.numberOfTokens)
	if err_nt != nil {
		return err_nt
	}

	return decoder.Decode(&cs.numberOfUniqueTokens)
}
