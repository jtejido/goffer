package statistics

import (
	"bytes"
	"encoding/gob"
)

type EntryStatistics struct {
	termFrequency             float64
	documentFrequency         float64
	totalTermByPresence       float64
	uniqueTotalByTermPresence float64
	postinglist               PostingStatistics
}

func NewEntryStatistics() *EntryStatistics {
	es := new(EntryStatistics)
	return es
}

func (es EntryStatistics) GetTermFrequency() float64 {
	return es.termFrequency
}

func (es EntryStatistics) GetDocumentFrequency() float64 {
	return es.documentFrequency
}

func (es EntryStatistics) GetTotalByTermPresence() float64 {
	return es.totalTermByPresence
}

func (es EntryStatistics) GetUniqueTotalByTermPresence() float64 {
	return es.uniqueTotalByTermPresence
}

func (es EntryStatistics) GetPostingList() PostingStatistics {
	return es.postinglist
}

func (es *EntryStatistics) SetTermFrequency(value float64) {
	es.termFrequency = value
}

func (es *EntryStatistics) SetDocumentFrequency(value float64) {
	es.documentFrequency = value
}

func (es *EntryStatistics) SetTotalByTermPresence(value float64) {
	es.totalTermByPresence = value
}

func (es *EntryStatistics) SetUniqueTotalByTermPresence(value float64) {
	es.uniqueTotalByTermPresence = value
}

func (es *EntryStatistics) SetPostingList(value PostingStatistics) {
	es.postinglist = value
}

// Satisfying GobEncoder Interface from encoding/gob
func (es *EntryStatistics) GobEncode() ([]byte, error) {
	w := new(bytes.Buffer)
	encoder := gob.NewEncoder(w)
	err_tf := encoder.Encode(es.termFrequency)
	if err_tf != nil {
		return nil, err_tf
	}
	err_df := encoder.Encode(es.documentFrequency)
	if err_df != nil {
		return nil, err_df
	}
	err_ttp := encoder.Encode(es.totalTermByPresence)
	if err_ttp != nil {
		return nil, err_ttp
	}
	err_uttp := encoder.Encode(es.uniqueTotalByTermPresence)
	if err_uttp != nil {
		return nil, err_uttp
	}
	err_pl := encoder.Encode(es.postinglist)
	if err_pl != nil {
		return nil, err_pl
	}

	return w.Bytes(), nil
}

// Satisfying GobDecoder Interface from encoding/gob
func (es *EntryStatistics) GobDecode(buf []byte) error {
	r := bytes.NewBuffer(buf)
	decoder := gob.NewDecoder(r)
	err_tf := decoder.Decode(&es.termFrequency)
	if err_tf != nil {
		return err_tf
	}
	err_df := decoder.Decode(&es.documentFrequency)
	if err_df != nil {
		return err_df
	}
	err_ttp := decoder.Decode(&es.totalTermByPresence)
	if err_ttp != nil {
		return err_ttp
	}
	err_uttp := decoder.Decode(&es.uniqueTotalByTermPresence)
	if err_uttp != nil {
		return err_uttp
	}

	return decoder.Decode(&es.postinglist)
}
