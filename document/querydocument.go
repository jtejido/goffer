package document

import (
	. "bitbucket.org/jtejido/goffer/util"
	. "bitbucket.org/jtejido/goffer/feature"
)

type QueryDocument struct {
	Document TokensDocument
}


func (qd QueryDocument) GetDocument() TokensDocument {
	return qd.Document.GetDocument()
}

func (qd *QueryDocument) ApplyTransformation(transformer TransformationInterface) {
	qd.Document.ApplyTransformation(transformer)
}

func (qd QueryDocument) CountValues() FeatureVector {
    return qd.Document.CountValues()
}
