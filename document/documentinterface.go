package document

import (
	. "bitbucket.org/jtejido/goffer/feature"
)

type DocumentInterface interface {
	GetDocument() TokensDocument
	CountValues() FeatureVector
}
