package document

import (
	. "bitbucket.org/jtejido/goffer/feature"
	. "bitbucket.org/jtejido/goffer/util"
	. "bitbucket.org/jtejido/goffer/types"
	"unicode/utf8"
	"unicode"
	"sync"

)

type TokensDocument Tokens

func (tokensdocument TokensDocument) GetDocument() TokensDocument {
	return tokensdocument
}

// transformation for each tokens are done here, we'll filter out null ones too
func (tokensdocument *TokensDocument) ApplyTransformation(transformer TransformationInterface) {
	var wg sync.WaitGroup
	temp := make(TokensDocument)
	var r rune
	wg.Add(len(*tokensdocument))
	go func() {
		for k, v := range (*tokensdocument) {
			defer wg.Done()
	
			var token = make(chan []byte, len(v))
			token <- v
			transformer.Transform(wg, token)
			tok := <- token
			r, _ = utf8.DecodeRune(tok)
			if len(tok) > 0 && !unicode.IsSpace(r) {
				temp[k] = tok
			}
		}
	}()
	wg.Wait()

	(*tokensdocument) = temp
}

func (tokensdocument TokensDocument) CountValues() FeatureVector {

    duplicate_frequency := make(FeatureVector)

    for _, item := range tokensdocument {
    	tok := string(item)
    	
        _, exist := duplicate_frequency[tok]

        if exist {
            duplicate_frequency[tok] += 1 
        } else {
            duplicate_frequency[tok] = 1
        }
    }

    return duplicate_frequency
}
