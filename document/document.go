package document

import (
	. "bitbucket.org/jtejido/goffer/feature"
	. "bitbucket.org/jtejido/goffer/metadata"
	. "bitbucket.org/jtejido/goffer/util"
)

type Document struct {
	Document TokensDocument
	MetaData MetaData
}

func (d Document) GetDocument() TokensDocument {
	return d.Document.GetDocument()
}

func (d Document) GetMetaData() MetaData {
	return d.MetaData
}

func (d *Document) ApplyTransformation(transformer TransformationInterface) {
	d.Document.ApplyTransformation(transformer)
}

func (d Document) CountValues() FeatureVector {
    return d.Document.CountValues()
}
