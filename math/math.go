package math

import (
	. "bitbucket.org/jtejido/goffer/util/maps"
	. "bitbucket.org/jtejido/goffer/util/slice"
	"math"
)

type Math struct{}

/**
 * Euclidean norm (L2 norm)
 * ||x||2 = sqrt(x・x) // ・ is a dot product
 */
func (m Math) EuclideanNorm(a map[string]float64) float64 {
	return math.Sqrt(m.DotProduct(a, a))
}

func (m Math) Round(a, pow float64) float64 {
	p := math.Pow(10, pow)
	return math.Round(a * p) / p
}

/**
 * Dot product
 * a・b = summation{i=1,n}(a[i] * b[i])
 */
func (m Math) DotProduct(a map[string]float64, b map[string]float64) float64 {
	var dotProduct float64 = 0

	keysA := Map_keys(a)
	keysB := Map_keys(b)
	uniqueKeys := Slice_unique(append(keysA, keysB...))
	for _, v := range uniqueKeys {
		_, ok_a := a[v]
		_, ok_b := b[v]
		if ok_a && ok_b {
			dotProduct += (a[v] * b[v])
		}
	}

	return dotProduct
}

// log a(b) = log(b)/log(a)
func (m Math) Log(x, base float64) float64 {
	return math.Log(x) / math.Log(base)
}
