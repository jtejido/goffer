package normalizer

import "bytes"
import "sync"

type English struct {}

// Normalizer for English, lowercase is just as fine
func NewEnglish() *English {
	return new(English)
}

func (english English) Normalize(b []byte) []byte {
	return bytes.ToLower(b)
}

func (english English) Transform(wg sync.WaitGroup, token chan []byte) {
	token <- english.Normalize(<- token)
}