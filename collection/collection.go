package collection

import (
	. "bitbucket.org/jtejido/goffer/document"
	. "bitbucket.org/jtejido/goffer/util"
)

type Collection map[int]*Document

func (c Collection) AddDocument(document Document) {
	c[len(c)] = &document
}

func (c Collection) ApplyTransformation(transformer TransformationInterface) {
	for _, v := range c {
		v.ApplyTransformation(transformer)
	}
}

func (c Collection) GetDocuments() Collection {
	return c
}
