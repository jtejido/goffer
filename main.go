package main

import (
	. "bitbucket.org/jtejido/goffer/metadata"
	. "bitbucket.org/jtejido/goffer/tokenizer"
	. "bitbucket.org/jtejido/goffer/util"
	. "bitbucket.org/jtejido/goffer/util/file"
	. "bitbucket.org/jtejido/goffer/normalizer"
	. "bitbucket.org/jtejido/goffer/document"
	. "bitbucket.org/jtejido/goffer/index"
	. "bitbucket.org/jtejido/goffer/similarity"
	. "bitbucket.org/jtejido/goffer/search"
	. "bitbucket.org/jtejido/goffer/model"
	"fmt"
	"time"
	"os"
    "path/filepath"

)

func main() {

	// prepare files
	start := time.Now()
	stopword_path := "stopwords.txt"
	stopword_file, _ := Fopen(stopword_path)

	tokenizer := NewWhitespaceTokenizer()
	st := NewStopWords(tokenizer.Tokenize(stopword_file))
	english := NewEnglish()

	// transformationset
	transformers := []TransformationInterface{st, english} // add transformers as slice[] of TransformationInterface
	transformationset := NewTransformationSet()
	transformationset.Register(transformers)


	// testing indexwriter, indexmanager, index, statistics, featurevector and indexentry
	indexwriter, _ := NewIndexWriter("index/")
	searchDir := "cranfield_parsed"
    err := filepath.Walk(searchDir, func(path string, f os.FileInfo, err error) error {
    	if !f.IsDir() {
	    	file, _ := Fopen(path)
	        indexwriter.AddDocument(Document{Document: TokensDocument(tokenizer.Tokenize(file)), MetaData: MetaData{"title": f.Name()}})
    	}
        return nil
    })

    if err != nil {
    	fmt.Println(err)
    }
	indexwriter.ApplyTransformation(transformationset)
	indexwriter.Close()

	// indexreader
	indexreader, _ := NewIndexReader("index/basset_index.idx")

	// testing search and models
	query_toks := TokensDocument(tokenizer.Tokenize("what theoretical and experimental guides do we have as to turbulent couette flow behaviour ."))
	query := QueryDocument{Document: query_toks} // query is a form of document, but no metadata needed
    query.ApplyTransformation(transformationset)
	search := NewSearch(indexreader)
	search.Query(query) 
	search.Model(NewTfIdf())
	search.QueryModel(NewTfIdf())
	search.Similarity(NewTverskyIndex())
	test, _ := search.Search()
	fmt.Println(test["491"])
	elapsed := time.Since(start)
	fmt.Println(elapsed)
}