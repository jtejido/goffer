package feature

import (
	"sort"
)

type FeatureVector map[string]float64


func (fv *FeatureVector) AddTerm(term string, weight float64) {
	_, ok := (*fv)[term]
	if ok {
		(*fv)[term] += weight
	} else {
		(*fv)[term] = weight
	}
}

func (fv *FeatureVector) AddTerms(value FeatureVector) {
	for str, weight := range value {
		_, ok := (*fv)[str]
		if ok {
			(*fv)[str] += weight
		} else {
			(*fv)[str] = weight
		}
	}
}

func (fv *FeatureVector) RemoveTerm(term string) {
	_, ok := (*fv)[term]
	if ok {
		delete((*fv), term)
	}
}

func  (fv FeatureVector) GetLength() float64 {

    var sum float64 = 0

    for _, value := range fv {
        sum += value
    }

    return sum
}

func (fv FeatureVector) GetCount() float64 {
	return float64(len(fv))
}

func (fv FeatureVector) GetFeature() FeatureVector {
	return fv
}

func (fv FeatureVector) Snip(length int) FeatureVector {
	n := make(map[float64][]string)
	vec := make(FeatureVector)
	var a []float64
	for k, v := range fv {
		n[v] = append(n[v], k)
	}
	for k := range n {
		a = append(a, k)
	}
	sort.Sort(sort.Reverse(sort.Float64Slice(a)))
	for _, k := range a {
		if len(vec) >= length {
			break
		}
		for _, s := range n[k] {
			vec[s] = k
		}
	}

	return vec
}
