package feature

type FeatureInterface interface {
    GetFeature() FeatureVector
    GetLength() float64
    GetCount() float64
}