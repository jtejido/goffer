package model

import (
	. "bitbucket.org/jtejido/goffer/similarity"
	"math"
)

type TwoStageLM struct {
	BaseModel
}

/**
 * TwoStageLM is a class for ranking documents that explicitly captures the different influences of the query and document
 * collection on the optimal settings of retrieval parameters.
 * It involves two steps. Estimate a document language for the model, and Compute the query likelihood using the estimated
 * language model. (DirichletLM and JelinkedMercerLM)
 *
 * From Chengxiang Zhai and John Lafferty. 2002. Two-Stage Language Models for Information Retrieval.
 * @see http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.7.3316&rep=rep1&type=pdf
 *
 * In a nutshell, this is a generalization of JelinkedMercerLM and DirichletLM.
 * The default values used here are the same constants found from the two classes.
 * Thus, making λ = 1 and μ same value as DirichletLM Class resolves the score towards DirichletLM, while as making μ larger
 * and λ same value as JelinekMercerLM Class resolves the score towards JelinekMercerLM.
 */
func NewTwoStageLM() *TwoStageLM {
	tslm := new(TwoStageLM)
	tslm.BaseModel.QueryModel = NewTermFrequency()
	tslm.BaseModel.Metric = NewVectorSimilarity()
	return tslm
}

/**
 * Smoothed p(w|d) is ((1 - λ)(c(w|d) + (μp(w|C))) / (|d| + μ)) + λp(w|C));
 * Document dependent constant is (1-λ)|d| + μ / (|d| + μ)
 *
 * The term weight in a form of KL divergence is given by p(w|Q)log(p(w|d)/αp(w|C)) + log α where:
 * p(w|d) = the document model.
 * p(w|C) = the collection model.
 * p(w|Q) = the query model.
 * α = document dependent constant
 *
 * Thus it becomes log(1 + (λc(w|d) / ((1-λ)|d| + μ)p(w|C)) + log((1-λ)|d| + μ / (|d| + μ)).
 */
func (tslm TwoStageLM) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {
	mu := tslm.GetConstant("mu")
	lambda := tslm.GetConstant("lambda")
	document_constant := tslm.GetDocumentConstant(docLength, docUniqueLength)
	// smoothed probability of words unseen in the collection
	mle_c := tslm.GetTermFrequency() / tslm.GetNumberOfTokens()

	// log(1 + (((1 - lambda) * (tf + (mu * mle_c)) / (docLength + mu)) + (lambda * mle_c)))
	return math.Log(1+((lambda*tf)/(((1-lambda)*docLength+mu)*mle_c))) + math.Log(document_constant)
}

func (tslm *TwoStageLM) GetBaseModel() *BaseModel {
	return &tslm.BaseModel
}

func (tslm *TwoStageLM) GetConstant(constant string) float64 {
	if constant == "lambda" {
		return lambda
	}
	if constant == "mu" {
		return mu
	}

	return 0

}

func (tslm TwoStageLM) GetDocumentConstant(docLength float64, docUniqueLength float64) float64 {
	return ((tslm.GetConstant("lambda"))*docLength + tslm.GetConstant("mu")) / (docLength + tslm.GetConstant("mu"))
}
