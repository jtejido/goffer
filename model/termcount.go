package model


type TermCount struct {
	BaseModel
}

func NewTermCount() *TermCount {
	termcount := new(TermCount)
	return termcount
}

func (termcount TermCount) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {
	return tf
}

func (termcount *TermCount) GetBaseModel() *BaseModel {
	return &termcount.BaseModel
}