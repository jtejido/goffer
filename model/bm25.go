package model

import (
	. "bitbucket.org/jtejido/goffer/similarity"
	"math"
)

const (
	B  = 0.75
	K1 = 1.2
)

type BM25 struct {
	BaseModel
}

/**
 * BM25 is a class for ranking documents against a query.
 *
 * The implementation is based on the paper by Stephen E. Robertson, Steve Walker, Susan Jones,
 * Micheline Hancock-Beaulieu & Mike Gatford (November 1994).
 * @see http://trec.nist.gov/pubs/trec3/t3_proceedings.html.
 *
 * Some modifications have been made to allow for non-negative scoring as suggested here.
 * @see https://doc.rero.ch/record/16754/files/Dolamic_Ljiljana_-_When_Stopword_Lists_Make_the_Difference_20091218.pdf
 */
func NewBM25() *BM25 {
	bm25 := new(BM25)
	bm25.BaseModel.QueryModel = NewTermCount()
	bm25.BaseModel.Metric = NewVectorSimilarity()
	return bm25
}

func (bm25 BM25) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {

	idf := math.Log(1 + ((bm25.GetNumberOfDocuments() - bm25.GetDocumentFrequency() + 0.5) / (bm25.GetDocumentFrequency() + 0.5)))
	num := tf * (K1 + 1)
	denom := tf + K1*(1-B+B*(docLength/bm25.GetAverageDocumentLength()))

	return idf * (num / denom)
}

func (bm25 *BM25) GetBaseModel() *BaseModel {
	return &bm25.BaseModel
}