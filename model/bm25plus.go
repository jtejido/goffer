package model

import (
	. "bitbucket.org/jtejido/goffer/similarity"
	"math"
)

const (
	BM25P_D float64 = 1
)

type BM25Plus struct {
	BaseModel
}

/**
 * BM25 is a class for ranking documents against a query where we made use of a delta(δ) value of 1,
 * which modifies BM25 to account for an issue against penalizing long documents and allowing shorter ones to dominate.
 * The delta values assures BM25 to be lower-bounded.
 * @see http://sifaka.cs.uiuc.edu/~ylv2/pub/cikm11-lowerbound.pdf
 *
 * Some modifications have been made to allow for non-negative scoring as suggested here.
 * @see https://doc.rero.ch/record/16754/files/Dolamic_Ljiljana_-_When_Stopword_Lists_Make_the_Difference_20091218.pdf
 *
 * We made use of a delta(δ) value of 1, which modifies BM25 to account for an issue against
 * penalizing long documents and allowing shorter ones to dominate. The delta values assures BM25
 * to be lower-bounded. (This makes this class BM25+)
 * @see http://sifaka.cs.uiuc.edu/~ylv2/pub/cikm11-lowerbound.pdf
 */
func NewBM25Plus() *BM25Plus {
	bm25p := new(BM25Plus)
	bm25p.BaseModel.QueryModel = NewTermCount()
	bm25p.BaseModel.Metric = NewVectorSimilarity()
	return bm25p
}

func (bm25p BM25Plus) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {

	idf := math.Log(1 + ((bm25p.GetNumberOfDocuments() - bm25p.GetDocumentFrequency() + 0.5) / (bm25p.GetDocumentFrequency() + 0.5)))
	num := tf * (K1 + 1)
	denom := tf + K1*(1-B+B*(docLength/bm25p.GetAverageDocumentLength()))

	return idf * ((num / denom) + BM25P_D)
}

func (bm25p *BM25Plus) GetBaseModel() *BaseModel {
	return &bm25p.BaseModel
}