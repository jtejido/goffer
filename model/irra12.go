package model

import (
	. "bitbucket.org/jtejido/goffer/similarity"
	"math"
)

type IRRA12 struct {
	BaseModel
}

/**
 * HiemstraLM is a class for ranking documents against a query based on Hiemstra's PHD thesis for language
 * model.
 * @see https://pdfs.semanticscholar.org/67ba/b01706d3aada95e383f1296e5f019b869ae6.pdf
 */
func NewIRRA12() *IRRA12 {
	irra := new(IRRA12)
	irra.BaseModel.QueryModel = NewTermCount()
	irra.BaseModel.Metric = NewVectorSimilarity()
	return irra
}

func (irra IRRA12) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {
	bm := irra.GetBaseModel()
	score := 0.0

	// eij+
	expected := (bm.GetTermFrequency() * docLength) / bm.GetNumberOfTokens()
	expected_plus := ((bm.GetTermFrequency() + 1) * (docLength + 1)) / (bm.GetNumberOfTokens() + 1)

	if tf <= expected {
		return score
	}
	alpha := (docLength - tf) / docLength
	beta := (2 / 3) * ((tf + 1) / tf)
	// Λij
	suppress_junk := math.Pow(alpha, (3/4)) * math.Pow(beta, (1/4))
	// ∆(Iij)
	score += ((tf + 1) * math.Log2((tf+1)/math.Sqrt(expected_plus))) - (tf * math.Log2(tf/math.Sqrt(expected)))
	return score * suppress_junk
}

func (irra *IRRA12) GetBaseModel() *BaseModel {
	return &irra.BaseModel
}