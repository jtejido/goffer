package model

import (
	. "bitbucket.org/jtejido/goffer/similarity"
	"math"
)

type TfIdf struct {
	BaseModel
}

func NewTfIdf() *TfIdf {
	tfidf := new(TfIdf)
	tfidf.BaseModel.QueryModel = new(TfIdf)
	tfidf.BaseModel.Metric = NewCosineSimilarity()
	return tfidf
}

func (tfidf TfIdf) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {

	df := tfidf.GetDocumentFrequency()
	if df <= 0 {
		return 0
	}

	return tf * math.Log(1+(tfidf.GetNumberOfDocuments()/df))
}

func (tfidf *TfIdf) GetBaseModel() *BaseModel {
	return &tfidf.BaseModel
}