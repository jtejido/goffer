package model

import (
	. "bitbucket.org/jtejido/goffer/similarity"
	"math"
)

const (
	BSDS_B = 0.4
)

type BSDS struct {
	BaseModel
}

/**
 * BSDS is a class that implements the Binary Standard Document Score (BSDS) with document length normalization.
 *
 * The implementation is based on Ronan Cummins' paper:
 * A Standard Document Score for Information Retrieval.
 * @see http://dcs.gla.ac.uk/~ronanc/papers/cumminsICTIR13.pdf
 *
 *
 * @author Jericko Tejido <jtbibliomania@gmail.com>
 */
func NewBSDS() *BSDS {
	bsds := new(BSDS)
	bsds.BaseModel.QueryModel = NewTermCount()
	bsds.BaseModel.Metric = NewVectorSimilarity()
	return bsds
}

func (bsds BSDS) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {
	tf_num := tf * (K1 + 1)
	tf_denom := tf + K1*(1-BSDS_B+BSDS_B*(docLength/bsds.GetAverageDocumentLength()))
	tfr := tf_num / tf_denom
	num := tfr - (bsds.GetDocumentFrequency() / bsds.GetNumberOfDocuments())
	denom := math.Sqrt((bsds.GetDocumentFrequency() - (math.Pow(bsds.GetDocumentFrequency(), 2) / bsds.GetNumberOfDocuments())) / bsds.GetNumberOfDocuments())

	if denom <= 0 {
		return 0
	}

	return num / denom
}

func (bsds *BSDS) GetBaseModel() *BaseModel {
	return &bsds.BaseModel
}