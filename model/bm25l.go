package model

import (
	. "bitbucket.org/jtejido/goffer/similarity"
	"math"
)

const (
	D = 0.5
)

type BM25L struct {
	BaseModel
}

/**
 * BM25L is a work of Lv and Zhai to rewrite BM25 due to Singhal et al's observation for having it penalized
 * longer documents.
 *
 * When Documents Are Very Long, BM25 Fails! (Lv and Zhai).
 * @see http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.600.16&rep=rep1&type=pdf
 */
func NewBM25L() *BM25L {
	bm25l := new(BM25L)
	bm25l.BaseModel.QueryModel = NewTermCount()
	bm25l.BaseModel.Metric = NewVectorSimilarity()
	return bm25l
}

func (bm25l BM25L) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {

	idf := math.Log(bm25l.GetNumberOfDocuments() + 1/bm25l.GetDocumentFrequency() + 0.5)
	c := tf / (1 - B + B*(docLength/bm25l.GetAverageDocumentLength()))
	num := (K1 + 1) * (c + D)
	denom := K1 + (c + D)

	return idf * (num / denom)
}

func (bm25l *BM25L) GetBaseModel() *BaseModel {
	return &bm25l.BaseModel
}