package model

import (
	. "bitbucket.org/jtejido/goffer/similarity"
	. "bitbucket.org/jtejido/goffer/statistics"
)

type BaseModel struct {
	Metric     SimilarityInterface
	QueryModel WeightedModelInterface
	CS         *CollectionStatistics
	Stats      *EntryStatistics
}

func (basemodel *BaseModel) SetSimilarity(metric SimilarityInterface) {
	basemodel.Metric = metric
}

func (basemodel BaseModel) GetSimilarity() SimilarityInterface {
	return basemodel.Metric
}

func (basemodel *BaseModel) SetQueryModel(model WeightedModelInterface) {
	basemodel.QueryModel = model
}

func (basemodel BaseModel) GetQueryModel() WeightedModelInterface {
	return basemodel.QueryModel
}

func (basemodel *BaseModel) SetStats(stats *EntryStatistics) {
	basemodel.Stats = stats
}

func (basemodel *BaseModel) SetCollectionStatistics(cs *CollectionStatistics) {
	basemodel.CS = cs
}

func (basemodel BaseModel) GetCollectionStatistics() *CollectionStatistics {
	return basemodel.CS
}

func (basemodel BaseModel) GetTermFrequency() float64 {
	return basemodel.Stats.GetTermFrequency()
}

func (basemodel BaseModel) GetDocumentFrequency() float64 {
	return basemodel.Stats.GetDocumentFrequency()
}

func (basemodel BaseModel) GetAverageDocumentLength() float64 {
	return basemodel.GetCollectionStatistics().GetAverageDocumentLength()
}

func (basemodel BaseModel) GetNumberOfTokens() float64 {
	return basemodel.GetCollectionStatistics().GetNumberOfTokens()
}

func (basemodel BaseModel) GetNumberOfUniqueTerms() float64 {
	return basemodel.GetCollectionStatistics().GetNumberOfUniqueTokens()
}

func (basemodel BaseModel) GetNumberOfDocuments() float64 {
	return basemodel.GetCollectionStatistics().GetNumberOfDocuments()
}

func (basemodel BaseModel) GetTotalByTermPresence() float64 {
	return basemodel.Stats.GetTotalByTermPresence()
}

func (basemodel BaseModel) GetUniqueTotalByTermPresence() float64 {
	return basemodel.Stats.GetUniqueTotalByTermPresence()
}
