package model

import (
	. "bitbucket.org/jtejido/goffer/similarity"
	"math"
)

const (
	lambda float64 = 0.7
)

type JelinekMercerLM struct {
	BaseModel
}

/**
 * JelinekMercerLM is a class for ranking documents against a query based on Linear interpolation of the maximum
 * likelihood model.
 *
 * From Chengxiang Zhai and John Lafferty. 2001. A study of smoothing methods for language models applied
 * to Ad Hoc information retrieval. In Proceedings of the 24th annual international ACM SIGIR conference on
 * Research and development in information retrieval (SIGIR '01).
 * @see http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.94.8019&rep=rep1&type=pdf
 * The value for λ is generally very small (0.1) for title queries and around 0.7 for verbose. Making it 1 makes term weight tends toward
 * zero.
 */
func NewJelinekMercerLM() *JelinekMercerLM {
	jmlm := new(JelinekMercerLM)
	jmlm.BaseModel.QueryModel = NewTermFrequency()
	jmlm.BaseModel.Metric = NewVectorSimilarity()
	return jmlm
}

/**
 * Smoothed p(w|d) is (1 - λ)p(w|d) + λp(w|C).
 * Document dependent constant is λ
 *
 * The term weight in a form of KL divergence is given by p(w|Q)log(p(w|d)/αp(w|C)) + log α where:
 * p(w|d) = the document model.
 * p(w|C) = the collection model.
 * p(w|Q) = the query model.
 * α = document dependent constant
 *
 * Thus it becomes log(1 + ((1 - λ)p(w|d) / λp(w|C))) + log(λ).
 */
func (jmlm JelinekMercerLM) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {

	constant := jmlm.GetConstant()
	document_constant := jmlm.GetDocumentConstant(docLength, docUniqueLength)
	// smoothed probability of words seen in the collection
	mle_c := jmlm.GetTermFrequency() / jmlm.GetNumberOfTokens()
	// smoothed probability of words seen in the document
	mle_d := tf / docLength

	// log(1 + ( (1 - lambda) * mle_d + (lambda * mle_c)) )
	return math.Log(1+(((1-constant)*mle_d)/(constant*mle_c))) + math.Log(document_constant)
}

func (jmlm *JelinekMercerLM) GetBaseModel() *BaseModel {
	return &jmlm.BaseModel
}

func (jmlm *JelinekMercerLM) GetConstant() float64 {
	return lambda
}

func (jmlm JelinekMercerLM) GetDocumentConstant(docLength float64, docUniqueLength float64) float64 {
	return jmlm.GetConstant()
}
