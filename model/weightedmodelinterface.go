package model


type WeightedModelInterface interface {
    GetScore(tf float64, docLength float64, docUniqueLength float64) float64
    GetBaseModel() *BaseModel
}