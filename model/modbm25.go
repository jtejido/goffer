package model

import (
	. "bitbucket.org/jtejido/goffer/similarity"
	"math"
)

type ModBM25 struct {
	BaseModel
}

/**
 * ModBM25 is a modified version of BM25 that ensures negative IDF don't violate Term-Frequency, Length Normalization and
 * TF-LENGTH Constraints by using Robertson-Sparck Idf.
 *
 * The implementation is based on the paper by Fang Et al.,
 * @see http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.59.1189&rep=rep1&type=pdf
 */
func NewModBM25() *ModBM25 {
	modbm25 := new(ModBM25)
	modbm25.BaseModel.QueryModel = NewTermCount()
	modbm25.BaseModel.Metric = NewVectorSimilarity()
	return modbm25
}

func (modbm25 ModBM25) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {

	idf := math.Log((modbm25.GetNumberOfDocuments() + 1) / modbm25.GetDocumentFrequency())
	num := tf * (K1 + 1)
	denom := tf + K1*(1-B+B*(docLength/modbm25.GetAverageDocumentLength()))

	return (num / denom) * idf
}

func (modbm25 *ModBM25) GetBaseModel() *BaseModel {
	return &modbm25.BaseModel
}