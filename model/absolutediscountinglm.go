package model

import (
	. "bitbucket.org/jtejido/goffer/similarity"
	"math"
)

const (
	delta float64 = 0.7
)

type AbsoluteDiscountingLM struct {
	BaseModel
}

/**
 * AbsoluteDiscountingLM is a class for ranking documents against a query by lowering down the probability of seen words by
 * subtracting a constant from their counts.
 *
 * The effect of this is that the events with the lowest counts are discounted relatively more than those with higher counts.
 * From Chengxiang Zhai and John Lafferty. 2001. A study of smoothing methods for language models applied
 * to Ad Hoc information retrieval. In Proceedings of the 24th annual international ACM SIGIR conference on
 * Research and development in information retrieval (SIGIR '01).
 * http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.94.8019&rep=rep1&type=pdf
 * The optimal value for 𝛿 tends to be around 0.7
 */
func NewAbsoluteDiscountingLM() *AbsoluteDiscountingLM {
	adlm := new(AbsoluteDiscountingLM)
	adlm.BaseModel.QueryModel = NewTermFrequency()
	adlm.BaseModel.Metric = NewVectorSimilarity()
	return adlm
}

/**
 * Smoothed p(w|d) is max(c(w|d) - 𝛿, 0) / ∑c(w|d) + (𝛿|d'| / |d|)p(w|C)
 * Document dependent constant is 𝛿|d'| / |d| where |d'| is number of unique terms in a document
 *
 * The term weight in a form of KL divergence is given by p(w|Q)log(p(w|d)/αp(w|C)) + log α where:
 * p(w|d) = the document model.
 * p(w|C) = the collection model.
 * p(w|Q) = the query model.
 * α = document dependent constant
 *
 * Thus it becomes log(1 + (c(w|d) - 𝛿 / 𝛿|d'|p(w|C)) + log(𝛿|d'| / |d|).
 */
func (adlm AbsoluteDiscountingLM) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {

	constant := adlm.GetConstant()
	document_constant := adlm.GetDocumentConstant(docLength, docUniqueLength)
	mle_c := adlm.GetTermFrequency() / adlm.GetNumberOfTokens()

	// log(1 + ((max(tf - delta, 0) / docLength) + (((delta * docUniqueLength) / docLength) * mle_c)))
	return math.Log(1+((tf-constant)/(constant*docUniqueLength*mle_c))) + math.Log(document_constant)
}

func (adlm *AbsoluteDiscountingLM) GetBaseModel() *BaseModel {
	return &adlm.BaseModel
}

func (adlm *AbsoluteDiscountingLM) GetConstant() float64 {
	return delta
}

func (adlm AbsoluteDiscountingLM) GetDocumentConstant(docLength float64, docUniqueLength float64) float64 {
	return (adlm.GetConstant() * docUniqueLength) / docLength
}