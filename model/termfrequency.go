package model


type TermFrequency struct {
	BaseModel
}

func NewTermFrequency() *TermFrequency {
	termfrequency := new(TermFrequency)
	return termfrequency
}

func (termfrequency TermFrequency) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {
	return tf / docLength
}

func (termfrequency *TermFrequency) GetBaseModel() *BaseModel {
	return &termfrequency.BaseModel
}