package model

import (
	. "bitbucket.org/jtejido/goffer/similarity"
	"math"
)

type AtireBM25 struct {
	BaseModel
}

/**
 * ATIRE BM25 is a class that uses Robertson-Walker IDF instead of the original Robertson-Sparck IDF.
 *
 * Towards an Efficient and Effective Search Engine (Trotman, Jia, Crane).
 * SIGIR 2012 Workshop on Open Source Information Retrieval.
 * @see http://opensearchlab.otago.ac.nz/paper_4.pdf
 */
func NewAtireBM25() *AtireBM25 {
	atbm25 := new(AtireBM25)
	atbm25.BaseModel.QueryModel = NewTermCount()
	atbm25.BaseModel.Metric = NewVectorSimilarity()
	return atbm25
}

func (atbm25 AtireBM25) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {

	idf := math.Log(atbm25.GetNumberOfDocuments() / atbm25.GetDocumentFrequency())
	num := tf * (K1 + 1)
	denom := tf + K1*(1-B+B*(docLength/atbm25.GetAverageDocumentLength()))

	return idf * (num / denom)
}

func (atbm25 *AtireBM25) GetBaseModel() *BaseModel {
	return &atbm25.BaseModel
}

