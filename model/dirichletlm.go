package model

import (
	. "bitbucket.org/jtejido/goffer/similarity"
	"math"
)

const (
	mu float64 = 2500
)

type DirichletLM struct {
	BaseModel
}

/**
 * DirichletLM is a class for ranking documents against a query based on Bayesian smoothing with
 * Dirichlet Prior for language modelling.
 *
 * From Chengxiang Zhai and John Lafferty. 2001. A study of smoothing methods for language models applied
 * to Ad Hoc information retrieval. In Proceedings of the 24th annual international ACM SIGIR conference on
 * Research and development in information retrieval (SIGIR '01).
 * @see http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.94.8019&rep=rep1&type=pdf
 * The optimal for μ appears to have a wide range (500-10000).
 */
func NewDirichletLM() *DirichletLM {
	dlm := new(DirichletLM)
	dlm.BaseModel.QueryModel = NewTermFrequency()
	dlm.BaseModel.Metric = NewVectorSimilarity()
	return dlm
}

/**
 * Smoothed p(w|d) is c(w|d) + μp(w|C) / ∑c(w|d) + μ
 * Document dependent constant is μ / μ + ∑c(w|d)
 *
 * The term weight in a form of KL divergence is given by p(w|Q)log(p(w|d)/αp(w|C)) + log α where:
 * p(w|d) = the document model.
 * p(w|C) = the collection model.
 * p(w|Q) = the query model.
 * α = document dependent constant
 *
 * Thus it becomes log(1 + (c(w|d) / μp(w|C)) + log(μ/μ+∑c(w|d)).
 */
func (dlm DirichletLM) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {

	constant := dlm.GetConstant()
	document_constant := dlm.GetDocumentConstant(docLength, docUniqueLength)
	// smoothed probability of words seen in the collection

	mle_c := dlm.GetTermFrequency() / dlm.GetNumberOfTokens()

	// log(1 + (tf + mu * mle_c) / (docLength + mu))
	return math.Log(1+(tf/(constant*mle_c))) + math.Log(document_constant)
}

func (dlm *DirichletLM) GetBaseModel() *BaseModel {
	return &dlm.BaseModel
}

func (dlm *DirichletLM) GetConstant() float64 {
	return mu
}

func (dlm DirichletLM) GetDocumentConstant(docLength float64, docUniqueLength float64) float64 {
	return dlm.GetConstant() / (dlm.GetConstant() + docLength)
}
