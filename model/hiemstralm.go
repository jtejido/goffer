package model

import (
	. "bitbucket.org/jtejido/goffer/similarity"
	"math"
)

const (
	C float64 = 0.15
)

type HiemstraLM struct {
	BaseModel
}

/**
 * HiemstraLM is a class for ranking documents against a query based on Hiemstra's PHD thesis for language
 * model.
 * @see https://pdfs.semanticscholar.org/67ba/b01706d3aada95e383f1296e5f019b869ae6.pdf
 */
func NewHiemstraLM() *HiemstraLM {
	hlm := new(HiemstraLM)
	hlm.BaseModel.QueryModel = NewTermCount()
	hlm.BaseModel.Metric = NewVectorSimilarity()
	return hlm
}

func (hlm HiemstraLM) GetScore(tf float64, docLength float64, docUniqueLength float64) float64 {

	return math.Log(1 + ((C * tf * hlm.GetTotalByTermPresence()) / ((1 - C) * hlm.GetDocumentFrequency() * docLength)))
}

func (hlm *HiemstraLM) GetBaseModel() *BaseModel {
	return &hlm.BaseModel
}