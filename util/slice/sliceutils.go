package slice

func Slice_count_values(list []string) map[string]float64 {

    duplicate_frequency := make(map[string]float64)

    for _, item := range list {

        _, exist := duplicate_frequency[item]

        if exist {
            duplicate_frequency[item] += 1 
        } else {
            duplicate_frequency[item] = 1
        }
    }
    return duplicate_frequency
}


func Slice_unique(a []string) []string {
    u := make([]string, 0, len(a))
    x := make(map[string]bool)

    for _, val := range a {
        if _, ok := x[val]; !ok {
            x[val] = true
            u = append(u, val)
        }
    }

    return u
}

