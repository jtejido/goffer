package transformation

import "sync"

type TransformationInterface interface {
	Transform(wg sync.WaitGroup, token chan []byte)
}
