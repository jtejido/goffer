package file

import (
	"fmt"
	"os"
)

func Fopen(path string) (string, error) {
	stopword_file, err := os.OpenFile(path, os.O_RDWR, 0644)

	if err != nil { 
		os.Exit(0)
		return "", err
	}
	defer stopword_file.Close()

	fileinfo, err := stopword_file.Stat()
	if err != nil {
	  return "", err
	}
	filesize := fileinfo.Size()

	text := make([]byte, filesize)
	n, err := stopword_file.Read(text)
	if n > 0 {
		return string(text), nil
	} else {
		return "", fmt.Errorf("file is empty")
	}
}

