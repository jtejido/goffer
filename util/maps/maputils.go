package maps


func Map_keys(a map[string]float64) []string {
    var keys []string
    for key, _ := range a {
        keys = append(keys, key)
    }
    return keys
}

func Map_sum(list map[string]float64) float64 {

    var sum float64 = 0

    for _, value := range list {
        sum += value
    }

    return sum
}
