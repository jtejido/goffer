package transformation

import (
	. "bitbucket.org/jtejido/goffer/types"
	"bytes"
	"sync"

)

type StopWords struct {
	stopwords Tokens
}

func NewStopWords(stopwords Tokens) *StopWords {
	s := new(StopWords)
	s.stopwords = stopwords
	return s
}

// If the token is part of the stopword list, return empty string
func (stopwords StopWords) Transform(wg sync.WaitGroup, token chan []byte) {

	var tok []byte
	tok = <- token
	defer wg.Done()
	wg.Add(1)
	for _, v := range stopwords.stopwords {

		if bytes.Equal(v, tok) {
			tok = nil
		}
	}
	token <- tok

}

// Retrieve the stopword list for reference
func (stopwords StopWords) GetStopwords() Tokens {
	return stopwords.stopwords
}
