package transformation

import "sync"

type TransformationSet struct {
	transformers []TransformationInterface
}

func NewTransformationSet() *TransformationSet {
	tr := new(TransformationSet)
	return tr
}

func (tr *TransformationSet) Register(transformer []TransformationInterface) {
	for _, v := range transformer {
		tr.transformers = append(tr.transformers, v)
	}
}

func (tr TransformationSet) Transformers() []TransformationInterface {
	return tr.transformers
}

func (tr TransformationSet) Transform(wg sync.WaitGroup, token chan []byte) {
	
	wg.Add(len(tr.transformers))
	for _, v := range tr.transformers {
		defer wg.Done()
		v.Transform(wg, token)
	}
	
	
}
