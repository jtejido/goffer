package metadata

type MetaData map[string]string

func (metadata *MetaData) AddData(tag, data string) {
	(*metadata)[tag] = data
}

func (metadata MetaData) GetTag(tag string) (string, bool) {
	var w string
	v, ok := metadata[tag]
	if !ok {
		w = ""
	} else {
		w = v
	}

	return w, ok
}

func (metadata MetaData) GetData() MetaData {
	return metadata
}