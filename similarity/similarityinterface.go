package similarity

import (
	. "bitbucket.org/jtejido/goffer/feature"
)

type SimilarityInterface interface {
	Similarity(a FeatureVector, b FeatureVector) float64
}