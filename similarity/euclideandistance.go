package similarity

import (
	. "bitbucket.org/jtejido/goffer/feature"
	. "bitbucket.org/jtejido/goffer/math"
)

type EuclideanDistance struct{}

func NewEuclideanDistance() *EuclideanDistance {
	ed := new(EuclideanDistance)
	return ed
}

func (ed EuclideanDistance) Similarity(a FeatureVector, b FeatureVector) float64 {
	if a == nil || b == nil {
		return 0
	}

	m := Math{}
	r := make(map[string]float64)
	for a_k, a_v := range a {
        r[a_k] = a_v
    }
    for b_k, b_v := range b {
        if _, ok := r[b_k]; ok {
            r[b_k] -= b_v
        } else {
            r[b_k] = b_v
        }
    }
    
    return m.EuclideanNorm(r)

}
