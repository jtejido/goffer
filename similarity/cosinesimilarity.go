package similarity

import (
	. "bitbucket.org/jtejido/goffer/math"
	. "bitbucket.org/jtejido/goffer/feature"
)

type CosineSimilarity struct{}

func NewCosineSimilarity() *CosineSimilarity {
	cs := new(CosineSimilarity)
	return cs
}

func (cs CosineSimilarity) Similarity(a FeatureVector, b FeatureVector) float64 {
	if a == nil || b == nil {
		return 0
	}

	m := Math{}

	normA := m.EuclideanNorm(a)
	normB := m.EuclideanNorm(b)

	if (normA * normB) <= 0 {
		return 0
	}

	return m.Round(m.DotProduct(a, b) / (normA * normB), 3)

}
