package similarity

import (
	. "bitbucket.org/jtejido/goffer/math"
	. "bitbucket.org/jtejido/goffer/feature"
	. "bitbucket.org/jtejido/goffer/util/maps"
	. "bitbucket.org/jtejido/goffer/util/slice"
	"math"
)

type JaccardSimilarity struct{}

func NewJaccardSimilarity() *JaccardSimilarity {
	js := new(JaccardSimilarity)
	return js
}

func (js JaccardSimilarity) Similarity(a FeatureVector, b FeatureVector) float64 {
	if a == nil || b == nil {
		return 0
	}

	m := Math{}

	vs := NewVectorSimilarity()

	dp := vs.Similarity(a, b)

	b_sum := 0.0

	a_sum := 0.0

	keysA := Map_keys(a)
	keysB := Map_keys(b)
	uniqueKeys := Slice_unique(append(keysA, keysB...))

	for _, v := range uniqueKeys {
		_, ok_a := a[v]
		_, ok_b := b[v]
		if ok_a {
			a_sum += (math.Pow(a[v], 2))
		}

		if ok_b {
			b_sum += (math.Pow(b[v], 2))
		}
	}



	return m.Round(dp / (b_sum + a_sum - dp), 3)

}
