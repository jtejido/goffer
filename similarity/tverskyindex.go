package similarity

import (
	. "bitbucket.org/jtejido/goffer/math"
	. "bitbucket.org/jtejido/goffer/feature"
	. "bitbucket.org/jtejido/goffer/util/maps"
	. "bitbucket.org/jtejido/goffer/util/slice"
	"math"
)

type TverskyIndex struct{}

func NewTverskyIndex() *TverskyIndex {
	ti := new(TverskyIndex)
	return ti
}

func (ti TverskyIndex) Similarity(a FeatureVector, b FeatureVector) float64 {
	if a == nil || b == nil {
		return 0
	}

	m := Math{}

	vs := NewVectorSimilarity()

	dp := vs.Similarity(a, b)


	a_diff := 0.0

	b_diff := 0.0

	keysA := Map_keys(a)
	keysB := Map_keys(b)
	uniqueKeys := Slice_unique(append(keysA, keysB...))

	for _, v := range uniqueKeys {
		_, ok_a := a[v]
		_, ok_b := b[v]

		if ok_a && !ok_b {
			a_diff += a[v]
		}

		if ok_b && !ok_a {
			b_diff += b[v]
		}
	}

	min := math.Min(a_diff,b_diff)
    max := math.Max(a_diff,b_diff)

	return m.Round(dp / (dp + (1 * (0.5 * min + max * (1 - 0.5)))), 3)

}
