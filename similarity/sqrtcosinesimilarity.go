package similarity

import (
	. "bitbucket.org/jtejido/goffer/feature"
	. "bitbucket.org/jtejido/goffer/util/maps"
	. "bitbucket.org/jtejido/goffer/util/slice"
	. "bitbucket.org/jtejido/goffer/math"
	"math"
)

type SqrtCosineSimilarity struct{}

func NewSqrtCosineSimilarity() *SqrtCosineSimilarity {
	scs := new(SqrtCosineSimilarity)
	return scs
}

func (scs SqrtCosineSimilarity) Similarity(a FeatureVector, b FeatureVector) float64 {
	if a == nil || b == nil {
		return 0
	}
	m := Math{}
	dotProduct := 0.0
	v1_norm := 0.0
	v2_norm := 0.0
	keysA := Map_keys(a)
	keysB := Map_keys(b)
	uniqueKeys := Slice_unique(append(keysA, keysB...))
	for _, v := range uniqueKeys {
		_, ok_a := a[v]
		_, ok_b := b[v]
		if ok_a && ok_b {
			dotProduct += math.Sqrt(a[v] * b[v])
		}

		if ok_a {
			v1_norm += math.Sqrt(a[v]) * math.Sqrt(a[v])
		}

		if ok_b {
			v2_norm += math.Sqrt(b[v]) * math.Sqrt(b[v])
		}	
	}

	v1_norm = math.Sqrt(v1_norm)
    v2_norm = math.Sqrt(v2_norm)

    if (v1_norm == 0 || v2_norm == 0){
        return 0
    }

    return m.Round(dotProduct / (v1_norm * v2_norm), 3)

}
