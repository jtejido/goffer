package similarity

import (
	. "bitbucket.org/jtejido/goffer/math"
	. "bitbucket.org/jtejido/goffer/feature"
	. "bitbucket.org/jtejido/goffer/util/maps"
	. "bitbucket.org/jtejido/goffer/util/slice"
	"math"
)

type OverlapCoefficient struct{}

func NewOverlapCoefficient() *OverlapCoefficient {
	oc := new(OverlapCoefficient)
	return oc
}

func (oc OverlapCoefficient) Similarity(a FeatureVector, b FeatureVector) float64 {
	if a == nil || b == nil {
		return 0
	}

	m := Math{}

	vs := NewVectorSimilarity()

	dp := vs.Similarity(a, b)


	b_sum := 0.0

	a_sum := 0.0

	keysA := Map_keys(a)
	keysB := Map_keys(b)
	uniqueKeys := Slice_unique(append(keysA, keysB...))

	for _, v := range uniqueKeys {
		_, ok_a := a[v]
		_, ok_b := b[v]

		if ok_a {
			a_sum += a[v]
		}

		if ok_b {
			b_sum += b[v]
		}
	}

	return m.Round(dp / math.Min(a_sum,b_sum), 3)

}
