package similarity

import (
	. "bitbucket.org/jtejido/goffer/util/maps"
	. "bitbucket.org/jtejido/goffer/util/slice"
	. "bitbucket.org/jtejido/goffer/feature"
	. "bitbucket.org/jtejido/goffer/math"
)

type VectorSimilarity struct{}

func NewVectorSimilarity() *VectorSimilarity {
	vs := new(VectorSimilarity)
	return vs
}

func (vs VectorSimilarity) Similarity(a FeatureVector, b FeatureVector) float64 {
	if a == nil || b == nil {
		return 0
	}

	m := Math{}
	var dotProduct float64 = 0

	keysA := Map_keys(a)
	keysB := Map_keys(b)
	uniqueKeys := Slice_unique(append(keysA, keysB...))

	for _, v := range uniqueKeys {
		_, ok_a := a[v]
		_, ok_b := b[v]
		if ok_a && ok_b {
			dotProduct += (a[v] * b[v])
		}
	}

	return m.Round(dotProduct, 3)

}
